# README #

## Part of [WasThere project](https://bitbucket.org/vojtisek_michal/wasthere/overview).

Application for Raspberry pi (tested on raspberry pi 3) searching for iBeacon around. When beacon is detected its MAC is uploaded to [AT&T M2X Cloud](https://m2x.att.com).
Based on [SwitchDoc Labs's blescanner](https://github.com/switchdoclabs/iBeacon-Scanner-).

## Dependences
 * PyBluez
 * m2x-python

### Before use set Environment variables (values are from M2X cloud):

 * M2X_KEY
 * M2X_DEVICE_ID
 * M2X_STREAM

## Setting up 

```
# bluez
sudo apt-get install bluez-tools

# bluez headers
sudo apt-get install libbluetooth-dev

# python headers
sudo apt-get install python-dev

# install pipi
sudo apt-get install python-pip

# PyBluez
sudo pip install PyBluez


#set sys vars

export M2X_KEY=<YOUR_M2X_API_KEY>
export M2X_DEVICE_ID=<YOUR_M2X_DEVICE_ID>
export M2X_STREAM=<YOUR_M2X_STREAM_ID>

```
