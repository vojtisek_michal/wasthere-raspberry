#!/usr/bin/env python

'''
iBeacon scanner which uploading beacons uuid to M2X cloud.
Based on SwitchDoc Labs's blescanner:
https://github.com/switchdoclabs/iBeacon-Scanner-

dependes on PyBluez, m2x-python

Before use set Environmet varibles:

export M2X_KEY=
export M2X_DEVICE_ID=
export M2X_STREAM=

'''

import sys, os
import datetime, time
import argparse

import bluetooth._bluetooth as bluez
from iBeaconScanner import blescan

from m2x.client import M2XClient


def main(dpr, slp, verbose=False):
	# device configuration is read from system envrionment varialles
	API_KEY = os.environ.get('M2X_KEY')
	DEVICE_ID = os.environ.get('M2X_DEVICE_ID')
	STREAM = os.environ.get('M2X_STREAM')

	if not API_KEY:
		print API_KEY
		print "error: no or empty M2X_KEY system variable set"
		sys.exit(1)

	if not DEVICE_ID:
		print "error: no or empty M2X_DEVICE_ID system variable set"
		sys.exit(1)

	if not STREAM:
		print "error: no or empty M2X_STREAM system variable set"
		sys.exit(1)

	try:
		# prepare M2X device
		client = M2XClient(key=API_KEY)
		device = client.device(DEVICE_ID)
		stream = device.stream(STREAM)

	except:
		print "Error: Can not initialize M2X!"
		sys.exit(1)


	# Init BLE
	dev_id = 0

	try:
		sock = bluez.hci_open_dev(dev_id)


	except:
		print "Error: Can not access bluetooth device..."
		sys.exit(1)

	blescan.hci_le_set_scan_parameters(sock)
	blescan.hci_enable_le_scan(sock)

	try:
		print "Scanning ... (CTRL + C to exit)"

		while True:

			returnedList = blescan.parse_events(sock, dpr)
			values = []

			if len(returnedList) > 0:
				print len(returnedList), "iBeacon(s) found around!"

			for beacon in returnedList:
				# some Beacons are detected prepare data for M2X
				uid = beacon.split(',')[0]

				if verbose:
					print uid

				values.append(dict(
					timestamp=datetime.datetime.now(),
					value=uid
				))

			try:
				# push beacon uuid to M2X
				device.post_updates(values={STREAM: values})
			except:
				print "Error: Can not update device stream!"

			time.sleep(slp)

	except KeyboardInterrupt:
		# quit
		sys.exit()



if __name__ == '__main__':
	parser = argparse.ArgumentParser(description = 'Detect iBeacons around and push them to M2X')
	parser.add_argument('--count', '-c', default=10, type=int, help='Number of beacons detected per detection round')
	parser.add_argument('--sleep', '-s', default=5, type=int, help='sleep between detection round in seconds')
	parser.add_argument('--verbose', '-v', action='store_const', const=True, help='Output extended informations ')
	args = parser.parse_args()

	v = False

	if args.verbose:
		v = True

	main(args.count, args.sleep, v)